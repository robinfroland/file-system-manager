package main.java.filemanager;

import java.util.List;

import main.java.logger.LogManager;

public class FileManager {
    FileOutputter fileOutputter = new FileOutputter();
    FileManipulator fileManipulator = new FileManipulator();
    LogManager logManager = new LogManager();

    public void outputFiles(String path) {
        outputFiles(path, "");
    }

    public void outputFiles(String path, String fileExtension) {
        long startTime = System.nanoTime();
        List<String> filenames = fileOutputter.getFiles(path, fileExtension);
        long endTime = System.nanoTime();
        String output = "\nFiles in " + path + "\n";
        if (!filenames.isEmpty()) {
            for (String file : filenames) {
                output += file + "\n";
                System.out.println(file);
            }
            logManager.log(output, (endTime - startTime) / 1000000);
        } else {
            output += "none";
        }
    }

    public void outputName() {
        long startTime = System.nanoTime();
        String name = fileManipulator.getName();
        long endTime = System.nanoTime();
        System.out.println("\nFile name:\n");
        System.out.println(name);
        logManager.log(name, (endTime - startTime) / 1000000);
    }

    public void outputFilesize() {
        long startTime = System.nanoTime();
        long filesize = fileManipulator.getFilesize();
        long endTime = System.nanoTime();
        String output = "\nFile size:\n";
        output += filesize;
        System.out.println(output);
        logManager.log(output, (endTime - startTime) / 1000000);
    }

    public void outputNumOfLines() {
        long startTime = System.nanoTime();
        int nLines = fileManipulator.getNumOfLines();
        long endTime = System.nanoTime();
        String output = "\n Number of lines:\n";
        output += nLines;
        System.out.println(output);
        logManager.log(output, (endTime - startTime) / 1000000);
    }

    public void outputWordExists(String word) {
        String output = "";
        long startTime = System.nanoTime();
        boolean exists = fileManipulator.wordExists(word);
        long endTime = System.nanoTime();
        if (exists) {
            output += "'" + word + "' does exist in this file";
        } else {
            output += "'" + word + "' does not exist in this file";
        }
        System.out.println(output);
        logManager.log(output, (endTime - startTime) / 1000000);
        ;
    }

    public void outputWordOccurence(String word) {
        long startTime = System.nanoTime();
        int nOccurences = fileManipulator.wordOccurence(word);
        long endTime = System.nanoTime();
        String output = "\n'" + word + "' occures " + nOccurences + " times in this text";
        System.out.println(output);
        logManager.log(output, (endTime - startTime) / 1000000);
    }

    public void outputLogFile() {
        if (fileOutputter.getLogFile() != null) {
            System.out.println();
        }
    }
}
