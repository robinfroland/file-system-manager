package main.java.filemanager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;

public class FileManipulator {
    String defaultTxtFile = "Dracula.txt";
    String basePath = "./src/main/resources/";

    File file = new File(basePath + defaultTxtFile);
    Path filePath = file.toPath();

    public String getName() {
        return file.getName();
    }

    public boolean renameFile(String newFilename) {
        boolean success = true;
        try {
            Files.move(filePath, filePath.resolveSibling(newFilename));
        } catch (FileAlreadyExistsException e) {
            System.out.println("A file with this name already exists!");
            success = false;
        } catch (NoSuchFileException e) {
            System.out.println("The file you are trying to modify does not exist");
            success = false;
        } catch (IOException e) {
            System.out.println("Something went wrong.");
            e.printStackTrace();
            success = false;
        }

        if (defaultTxtFile.equals(newFilename)) {
            System.out.println("The file you are trying to modify already has this name!");
            success = false;
        }
        return success;
    }

    public long getFilesize() {
        long nBytes = 0;

        // No need to close stream when used as a try-with-resource
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            int data = fileInputStream.read();

            // loop until
            while (data != -1) {
                // Increment number of bites so far
                nBytes++;
                // next data
                data = fileInputStream.read();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        long kilobytes = nBytes / 1024;
        return kilobytes;
    }

    public int getNumOfLines() {
        int nLines = 0;

        // No need to close stream when used as a try-with-resource
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            while ((bufferedReader.readLine()) != null) {
                // Increment number of lines for each line read
                nLines++;
            }
        } catch (FileNotFoundException error) {
            System.out.println("File not found");
            System.out.println(error.getMessage());
        } catch (IOException error) {
            nLines = -1;
            System.out.println("Something went wrong.");
            System.out.println(error.getMessage());
        }

        return nLines;
    }

    public boolean wordExists(String word) {
        boolean wordExists = false;

        String line;
        // No need to close stream when used as a try-with-resource
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            line = bufferedReader.readLine();
            // Loop until word is found or there are no more lines to be read
            while (!wordExists && line != null) {
                // Make comparison non case-sensitive and check if word exist in current line
                if (line.toLowerCase().contains(word.toLowerCase())) {
                    wordExists = true;
                }
                // Read next line
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException error) {
            System.out.println("File not found");
            System.out.println(error.getMessage());
        } catch (IOException error) {
            wordExists = false;
            System.out.println("Something went wrong.");
            System.out.println(error.getMessage());
        }

        return wordExists;
    }

    public int wordOccurence(String input) {
        String word = input.toLowerCase();
        int nOccurences = -1;

        String line;
        // No need to close stream when used as a try-with-resource
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            // Loop until there are no more lines to be read
            while ((line = bufferedReader.readLine()) != null) {
                // Check if line contains the word
                if (line.toLowerCase().contains(word)) {
                    // Use regex expression to split the line on non-word characters
                    String[] words = line.split("\\W");
                    for (String w : words) {
                        // Compare every word with input word and increment if equals
                        if (w.toLowerCase().equals(word)) {
                            nOccurences++;
                        }
                    }
                }
            }
        } catch (FileNotFoundException error) {
            System.out.println("File not found");
            System.out.println(error.getMessage());
        } catch (IOException error) {
            nOccurences = -1;
            System.out.println("Something went wrong.");
            System.out.println(error.getMessage());
        }

        return nOccurences;
    }
}
