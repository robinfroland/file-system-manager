package main.java.filemanager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileOutputter {
    File directory;

    public File getLogFile() {
        File log = new File("./log/txt");
        if (!log.exists()) {
            return null;
        }
        return log;

    }

    // If no extension is given
    public void getFiles(String path) {
        getFiles(path, "");
    }

    // Overload method to allow optional extension
    public List<String> getFiles(String path, String fileExtension) {
        this.directory = new File(path);
        List<String> filenames = new ArrayList<String>();

        // Check if the given directory exists
        if (!directory.exists()) {
            System.out.println("The directory you are trying to access does not exist");
        } else {
            File[] fileList = directory.listFiles();
            boolean typeExists = false;

            // Check if any files are present in directory
            if (fileList.length == 0) {
                System.out.println("There are no files in this directory");
            } else {
                // Iterate through all files in directory
                for (File file : fileList) {
                    String filename = file.getName();
                    String[] fileNameArr = filename.split("\\.");
                    // In case multiple dots are present in the filename
                    String extension = fileNameArr[fileNameArr.length - 1];

                    // Output files with matching extension if provided
                    if (extension.equals(fileExtension) || fileExtension.isEmpty()) {
                        filenames.add(filename);
                        typeExists = true;
                    }
                }

                if (!typeExists) {
                    System.out.println("There are no files in this directory matching that extension");
                }
            }
        }
        return filenames;
    }
}
