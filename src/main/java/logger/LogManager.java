package main.java.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class LogManager {
    File log = null;

    public LogManager() {
        createLogFile();

    }

    public void log(String entry, long executionTime) {

        try {
            FileWriter log = new FileWriter(this.log, true);
            log.append("\n\n\n-- "
                    + DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).format(ZonedDateTime.now()) + " --\n");
            log.append(entry);
            log.append("\n\nThe function took " + executionTime + "ms to execute.\n");
            log.close();

        } catch (IOException e) {
            System.out.println("Something went wrong");
            System.out.println(e.getMessage());
        }
    }

    private void writeHeader() {
        try {
            FileWriter log = new FileWriter(this.log, true);
            log.append("+-----------------------------------------+\n");
            log.append("|                   LOG                   |\n");
            log.append("+-----------------------------------------+\n");
            log.close();

        } catch (IOException e) {
            System.out.println("Something went wrong");
            System.out.println(e.getMessage());
        }
    }

    private void createLogFile() {
        try {
            File logFile = new File("./log.txt");
            this.log = logFile;
            if (!logFile.exists()) {
                writeHeader();
            }
        } catch (Exception e) {
            System.out.println("Something went wrong");
            System.out.println(e.getMessage());
        }
    }
}
