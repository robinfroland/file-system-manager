package main.java;

import main.java.inputmanager.InputManager;

/**
 * Main program
 */
public class Program {

    public static void main(String[] args) {
        InputManager inputManager = new InputManager();
        inputManager.initMenu();
    }
}