package main.java.inputmanager;

public class MenuGui {

    public void outputHeader() {
        System.out.println("+-----------------------------------------+");
        System.out.println("|          FILE SYSTEM MANAGER            |");
        System.out.println("+-----------------------------------------+");
    }

    public void outputOptions() {
        System.out.println("\n -- MENU --\n");
        System.out.println(" 1: List all file names");
        System.out.println(" 2: List all file names by file extension");
        System.out.println(" 3: Get information on text file");
        System.out.println(" 0: Quit");
    }

    public void outputExtensionOptions() {
        System.out.println("\n -- LIST FILES BY EXTENSION --\n");
        System.out.println(" 1: .txt");
        System.out.println(" 2: .png");
        System.out.println(" 3: .jpg");
        System.out.println(" 4: .jpeg");
        System.out.println(" 5: .jfif");
        System.out.println(" 6: Other");
    }

    public void outputInfoOptions() {
        System.out.println("\n -- GET FILE INFORMATION --\n");
        System.out.println(" 1: File name");
        System.out.println(" 2: File size");
        System.out.println(" 3: Number of lines");
        System.out.println(" 4: Existence of a spesific word");
        System.out.println(" 5: The number of times a spesific word occurs");
    }
}
