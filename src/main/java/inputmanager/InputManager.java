package main.java.inputmanager;

import main.java.filemanager.FileManager;

import java.util.Scanner;

public class InputManager {
    FileManager fileManager = new FileManager();
    String defaultTxtFile = "Dracula.txt";
    String basePath = "./src/main/resources/";

    MenuGui gui = new MenuGui();
    boolean abort = false;

    Scanner input = new Scanner(System.in);
    Scanner inputFile;

    public void initMenu() {
        gui.outputHeader();

        while (!abort) {
            gui.outputOptions();
            int selectedOption = getSelection(0, 3);
            switch (selectedOption) {
                case 1:
                    fileManager.outputFiles(basePath);
                    actionComplete();
                    break;
                case 2:
                    gui.outputExtensionOptions();
                    extensionInputAction();
                    actionComplete();
                    break;
                case 3:
                    gui.outputInfoOptions();
                    infoInputAction();
                    actionComplete();
                    break;

                case 0:
                    abort();
                    break;
            }
        }
    }

    private void extensionInputAction() {
        int selectedOption = getSelection(1, 6);
        switch (selectedOption) {
            case 1:
                fileManager.outputFiles(basePath, "txt");
                break;
            case 2:
                fileManager.outputFiles(basePath, "png");
                break;
            case 3:
                fileManager.outputFiles(basePath, "jpg");
                break;
            case 4:
                fileManager.outputFiles(basePath, "jpeg");
                break;
            case 5:
                fileManager.outputFiles(basePath, "jpeg");
                break;
            case 6:
                System.out.print("\n Extension: ");
                fileManager.outputFiles(basePath, input.nextLine());
                System.out.println();
                break;
        }
    }

    private void infoInputAction() {
        int selectedOption = getSelection(1, 5);
        switch (selectedOption) {
            case 1:
                fileManager.outputName();
                break;
            case 2:
                fileManager.outputFilesize();
                break;
            case 3:
                fileManager.outputNumOfLines();
                break;
            case 4:
                System.out.print("\n Word: ");
                fileManager.outputWordExists(input.nextLine());
                System.out.println();
                break;
            case 5:
                System.out.print("\n Word: ");
                fileManager.outputWordOccurence(input.nextLine());
                System.out.println();
                break;
        }
    }

    private int getSelection(int min, int max) {
        int selectedOption = -1;

        while (selectedOption < min || selectedOption > max) {
            System.out.print(" > ");
            selectedOption = getInt();
        }
        input.nextLine();
        return selectedOption;
    }

    private int getInt() {
        while (!input.hasNextInt()) {
            System.out.println(" Invalid input. Please try again.");
            System.out.print(" > ");
            input.nextLine();
        }
        return input.nextInt();
    }

    private void actionComplete() {
        System.out.println("\n 1: Go back to main menu");
        System.out.println(" 0: Quit");
        int selectedOption = getSelection(0, 1);
        switch (selectedOption) {
            case 1:
                break;
            case 0:
                abort();
                break;
        }
    }

    private void abort() {
        System.out.println(" File system manager shutting down...\n");
        abort = true;
    }
}
