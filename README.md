# File Management System
A Java console application that can be used to manage and manipulate different kind of files.

## Getting started
Run the command "javac -d out $(find . -name "*.java") && java -cp out main.java.Program" from the root folder.

![](src/main/resources/filemanager.png)